class CreateRedeems < ActiveRecord::Migration
  def change
    create_table :redeems do |t|
      t.integer :account_id
      t.date :redeem_date
      t.decimal :redeemed_coins

      t.timestamps
    end
  end
end
