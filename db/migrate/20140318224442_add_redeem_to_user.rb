class AddRedeemToUser < ActiveRecord::Migration
  def change
    add_column :users, :has_redeemed, :boolean, default: false
    add_column :users, :last_redeem, :integer
  end
end
