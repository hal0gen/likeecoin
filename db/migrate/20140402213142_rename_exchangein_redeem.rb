class RenameExchangeinRedeem < ActiveRecord::Migration
  def change
  	rename_column :redeems, :exchange_rate_set_id, :redeem_round_id
  end
end
