class AddStatusToInfo < ActiveRecord::Migration
  def change
    add_column :infos, :statuses_count, :integer
    add_column :infos, :statuses_likes, :integer
  end
end
