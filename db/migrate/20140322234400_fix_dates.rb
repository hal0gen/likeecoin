class FixDates < ActiveRecord::Migration
  def change
  	change_column :accounts, :last_redeem, :datetime
  	remove_column :exchange_rate_sets, :set_date
  	remove_column :redeems, :redeem_date
  	remove_column :transactions, :transaction_date
  end
end
