class AddFieldsToExchangeRateSets < ActiveRecord::Migration
  def change
    add_column :exchange_rate_sets, :begins_at, :datetime
    add_column :exchange_rate_sets, :exchange_volume, :decimal
    add_column :exchange_rate_sets, :total_coins, :decimal
    add_column :exchange_rate_sets, :transactions_number, :integer
  end
end
