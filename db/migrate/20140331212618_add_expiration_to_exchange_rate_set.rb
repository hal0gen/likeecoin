class AddExpirationToExchangeRateSet < ActiveRecord::Migration
  def change
    add_column :exchange_rate_sets, :expires_at, :datetime
  end
end
