class RenameExchangeRateSet < ActiveRecord::Migration
  def change
  	rename_table :exchange_rate_sets, :redeem_rounds
  	rename_column :transactions, :transaction_amount, :amount
  end
end
