class AddExchangeRateSetToRedeem < ActiveRecord::Migration
  def change
    add_column :redeems, :exchange_rate_set_id, :integer
  end
end
