class CreateOwnersBanks < ActiveRecord::Migration
  def change
    create_table :owners_banks do |t|
      t.decimal :balance

      t.timestamps
    end
  end
end
