class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.decimal :balance
      t.boolean :has_redeemed
      t.date :last_redeem

      t.timestamps
    end
  end
end
