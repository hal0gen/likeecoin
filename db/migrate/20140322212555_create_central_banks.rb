class CreateCentralBanks < ActiveRecord::Migration
  def change
    create_table :central_banks do |t|
      t.decimal :generated_coins
      t.decimal :current_exchange_rate

      t.timestamps
    end
  end
end
