class RenameOtherTables < ActiveRecord::Migration
  def change
  	rename_column :redeems, :user_account_id, :account_id
  	rename_column :transactions, :debit_user_account_id, :debit_account_id
  	rename_column :transactions, :credit_user_account_id, :credit_account_id
  end
end
