class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.integer :debit_account_id
      t.integer :credit_account_id
      t.date :transaction_date
      t.decimal :transaction_amount
      t.text	:description

      t.timestamps
    end
  end
end
