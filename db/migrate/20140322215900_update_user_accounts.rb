class UpdateAccounts < ActiveRecord::Migration
  def change
  	change_column :accounts, :balance, :decimal, default: 0
    change_column :accounts, :has_redeemed, :boolean, default: false
  end
end
