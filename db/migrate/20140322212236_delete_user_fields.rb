class DeleteUserFields < ActiveRecord::Migration
  def change
  	remove_column :users, :likeecoins
  	remove_column :users, :has_redeemed
  	remove_column :users, :last_redeem
  end
end
