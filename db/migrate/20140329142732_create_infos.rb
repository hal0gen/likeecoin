class CreateInfos < ActiveRecord::Migration
  def change
    create_table :infos do |t|
      t.integer :user_id
      t.text :friends_uids
      t.integer :friends_count
      t.integer :links_likes
      t.integer :links_count
      t.integer :photos_likes
      t.integer :photos_count

      t.timestamps
    end
  end
end
