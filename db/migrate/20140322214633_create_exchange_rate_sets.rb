class CreateExchangeRateSets < ActiveRecord::Migration
  def change
    create_table :exchange_rate_sets do |t|
      t.date :set_date
      t.decimal :exchange_rate

      t.timestamps
    end
  end
end
