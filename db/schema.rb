# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140409224501) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: true do |t|
    t.decimal  "balance",      default: 0.0
    t.boolean  "has_redeemed", default: false
    t.datetime "last_redeem"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
  end

  create_table "central_banks", force: true do |t|
    t.decimal  "generated_coins"
    t.decimal  "current_exchange_rate"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "delayed_jobs", force: true do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "infos", force: true do |t|
    t.integer  "user_id"
    t.text     "friends_uids"
    t.integer  "friends_count"
    t.integer  "links_likes"
    t.integer  "links_count"
    t.integer  "photos_likes"
    t.integer  "photos_count"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "statuses_count"
    t.integer  "statuses_likes"
  end

  create_table "owners_banks", force: true do |t|
    t.decimal  "balance"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "redeem_rounds", force: true do |t|
    t.decimal  "exchange_rate"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "expires_at"
    t.datetime "begins_at"
    t.decimal  "exchange_volume"
    t.decimal  "total_coins"
    t.integer  "transactions_number"
  end

  create_table "redeems", force: true do |t|
    t.integer  "account_id"
    t.decimal  "redeemed_coins"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "redeem_round_id"
  end

  create_table "transactions", force: true do |t|
    t.integer  "debit_account_id"
    t.integer  "credit_account_id"
    t.decimal  "amount"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image"
    t.string   "email"
    t.string   "gender"
    t.string   "location"
    t.string   "locale"
  end

end
