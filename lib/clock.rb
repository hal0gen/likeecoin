require 'clockwork'
require './config/boot'
require './config/environment'

include Clockwork


every(1.day, 'redeem.launch', :at => '08:00') { Delayed::Job.enqueue RedeemJob.new }
every(1.day, 'redeem.launch', :at => '20:00') { Delayed::Job.enqueue RedeemJob.new }
