class User < ActiveRecord::Base

  has_one :account
  has_one :info
  has_many :transactions, through: :account

  def self.from_omniauth(auth)
    where(auth.slice(:provider, :uid)).first_or_initialize.tap do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.name = auth.info.name
      user.email = auth.info.email
      user.image = auth.info.image
      user.gender = auth.extra.raw_info.gender
      user.location = auth.info.location
      user.locale = auth.extra.raw_info.locale
      user.oauth_token = auth.credentials.token
      user.oauth_expires_at = Time.at(auth.credentials.expires_at)
      user.save!
      account = Account.where( user_id: user.id ).first_or_initialize
      account.save!
    end
  end

  def last_redeem
    if self.account.has_redeemed
      self.account.last_redeem.to_time
    else
      6.months.ago.to_time
    end
  end

  def facebook
    @facebook ||= Koala::Facebook::API.new(oauth_token)
    block_given? ? yield(@facebook) : @facebook
  rescue Koala::Facebook::APIError => e
    logger.info e.to_s
    nil 
  end

  def recent_transactions
    transactions.(created_at: :desc).limit(5)
  end

  def user_friends
    User.where(uid: info.friends_uids).order(:name)
  end

  

end

