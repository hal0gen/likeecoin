class Transaction < ActiveRecord::Base
	has_one :account, foreign_key: 'debit_account_id'
  	has_one :account, foreign_key: 'credit_account_id'
end
