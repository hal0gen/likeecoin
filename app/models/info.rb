class Info < ActiveRecord::Base
	belongs_to :user
	serialize :friends_uids

	def total_likes
    	links_likes + photos_likes + statuses_likes
  	end
end
