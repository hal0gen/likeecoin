class Account < ActiveRecord::Base
	has_many :redeems
	has_many :debit_transactions, foreign_key: 'debit_account_id', class_name: 'Transaction'
	has_many :credit_transactions, foreign_key: 'credit_account_id', class_name: 'Transaction'
	belongs_to :user
	belongs_to :debit_account, :class_name => 'Transaction' , :foreign_key => 'debit_account_id'
  	belongs_to :credit_account, :class_name => 'Transaction' , :foreign_key => 'credit_account_id'

  	validates :user, presence: true

  	def transactions_list
  		debit_transactions + credit_transactions
  	end

  	def recent_transactions
  		transactions_list.(created_at: :desc).limit(5)
  	end


end
