class RedeemRound < ActiveRecord::Base
	has_many :redeems
	validates :begins_at, :expires_at, presence: true 
end
