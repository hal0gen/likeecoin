// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require foundation
//= require turbolinks
//= require jquery.countdown
//= require_tree .

$(function(){ 
	$(document).foundation(); 

	$('#myModal').foundation('reveal', 'open', { 
		animation: 'fadeAndPop',
		animation_speed: 250
	});
	var now = new Date();
	redeemTime = $('.countdown').data('redeemtime');
	if (now.getHours() > 6 && now.getHours() < 18) { 
		var redeemTime = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 22, 0, 0, 0);
		if (redeemTime - now < 0) { 
			redeemTime = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1, 22, 0, 0, 0); 
		}
	}
	else
		var redeemTime = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 10, 0, 0, 0);
		if (redeemTime - now < 0) { 
			redeemTime = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1, 10, 0, 0, 0); 
		}	
	$('.countdown').countdown({until: redeemTime, format: 'HMS', layout: '{hn} {hl}, {mn} {ml}, {sn} {sl}'});
});
