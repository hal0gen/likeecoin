class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  after_filter :set_access_control_headers

  def set_access_control_headers 
    headers['Access-Control-Allow-Origin'] = 'http://0.0.0.0:3000/' 
  end

  private
  
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def redeem_time
    RedeemRound.where("begins_at < ? AND expires_at > ?", Time.now(), Time.now()).present?
  end

  helper_method :current_user, :redeem_time

end
