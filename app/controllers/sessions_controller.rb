class SessionsController < ApplicationController
  
  def create
    user = User.from_omniauth(env["omniauth.auth"])
    session[:user_id] = user.id
    # user.reset_date if user.likes_expired
    get_user_info unless redeem_time
    redirect_to root_url
  end

  def redeem
    if redeem_time || !current_user.account.has_redeemed
      coins_amount = current_user.info.total_likes * CentralBank.first.current_exchange_rate 
      redeem = Redeem.new ( { account: current_user.account, redeemed_coins: coins_amount, redeem_round: RedeemRound.last } )
      redeem.save
      current_user.account.balance += coins_amount
      central_bank = CentralBank.first
      central_bank.generated_coins += coins_amount
      current_user.account.has_redeemed = true
      current_user.account.last_redeem = Time.now
      central_bank.save
      current_user.account.save
      get_user_info
      redirect_to root_url
    end
  end

  def refresh
    get_user_info
    redirect_to root_url
  end

  
  def transaction
    movement = Transaction.new ({ 
      amount: params[:amount].to_d, 
      debit_account_id: current_user.id, 
      credit_account_id: params[:receiver],
      description: params[:description]
       })
    debit = current_user.account
    debit.balance -= params[:amount].to_d
    credit = User.find(params[:receiver]).account
    credit.balance += params[:amount].to_d
    debit.save
    credit.save
    movement.save
    redirect_to root_url
  end

  def destroy
  	session[:user_id] = nil
    redirect_to root_url
  end

  def get_user_info
    links, photos, statuses, friends = current_user.facebook.batch do |batch_api|
      batch_api.fql_query('SELECT created_time, like_info FROM link WHERE owner = me() AND created_time > ' + current_user.last_redeem.to_i.to_s)
      batch_api.fql_query('SELECT like_info FROM photo WHERE owner = me() AND created > ' + current_user.last_redeem.to_i.to_s)
      batch_api.fql_query('SELECT like_info FROM status WHERE uid = me() AND time > ' + current_user.last_redeem.to_i.to_s) 
      batch_api.fql_query('SELECT uid2 FROM friend WHERE uid1 = me()')
    end
    Info.where(user_id: current_user.id).first_or_initialize.tap do |info|
      info.friends_uids = []
      info.links_likes, info.photos_likes, info.statuses_likes = 0, 0, 0
      friends.each {|f| info.friends_uids << f['uid2']} 
      info.friends_count = friends.length
      links.each {|l| info.links_likes += l['like_info']['like_count']}
      info.links_count = links.length
      photos.each {|p| info.photos_likes += p['like_info']['like_count']}
      info.photos_count = photos.length
      statuses.each {|s| info.statuses_likes += s['like_info']['like_count']}
      info.statuses_count = statuses.length
      info.save!
    end
  end

end