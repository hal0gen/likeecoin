class RedeemJob
	def perform
		last_redeem = RedeemRound.order("id DESC").first 
		exchange_volume = 0
    	redeemable_coins = 0
    	redeemable_likes = 0
    	variation = 0
    	total_coins = CentralBank.first.generated_coins 
        if last_redeem
            last_transactions = Transaction.where("created_at > :last_expire", last_expire: last_redeem.expires_at)
            last_transactions.each { |t| exchange_volume += t.amount }
    		if exchange_volume > 0 && last_redeem.exchange_volume > 0
    			variation = ( exchange_volume - last_redeem.exchange_volume ) / last_redeem.exchange_volume
    		end
    		if exchange_volume > 0 && last_redeem.exchange_volume == 0
    			variation = 0.05
    		end
    		if exchange_volume == 0
				exchange_rate = 0.05
			else			
	    		redeemable_coins = total_coins * variation
	    		User.all.find_each {|u| redeemable_likes += u.info.total_likes} if User.all.first.present?
	    		if redeemable_likes == 0 || redeemable_coins == 0 
	    			exchange_rate = 0.05 
	    		else
	    			exchange_rate = (redeemable_coins / redeemable_likes)
	    		end
	    	end
    	else
    		exchange_rate = 0.05
    	end

    	RedeemRound.new do |r|
  			r.begins_at = Time.now
  			r.expires_at = Time.now + 1.hour
			r.exchange_volume = exchange_volume
			r.total_coins = total_coins
			r.exchange_rate = exchange_rate
			r.save!
		end

		CentralBank.first.tap do |cb|
    		cb.current_exchange_rate = exchange_rate
    		cb.save!
		end
  	
    end
end
